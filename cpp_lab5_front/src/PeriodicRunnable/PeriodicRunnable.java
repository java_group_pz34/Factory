package PeriodicRunnable;

public abstract class PeriodicRunnable implements Runnable {
    protected long periodInMilliseconds;
    private boolean isWorking;

    protected PeriodicRunnable(long periodInMilliseconds) {
        this.periodInMilliseconds = periodInMilliseconds;
        isWorking = true;
    }

    @Override
    public void run() {
        try {
            while (isWorking) {
                Thread.sleep(periodInMilliseconds);
                runLogic();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected abstract void runLogic() throws InterruptedException;

    public long getPeriodInMilliseconds() {
        return periodInMilliseconds;
    }

    public void setPeriodInMilliseconds(long periodInMilliseconds) {
        this.periodInMilliseconds = periodInMilliseconds;
    }

    public boolean isWorking() {
        return isWorking;
    }

    public void setWorking(boolean working) {
        isWorking = working;
    }
}
