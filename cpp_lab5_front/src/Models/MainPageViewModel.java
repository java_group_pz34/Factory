package Models;

import Events.*;
import Factories.AccessoryFactory;
import Factories.CabFactory;
import Factories.CarFactory;
import Factories.EngineFactory;
import GUI.MainWindowFXController;
import Mediator.FactoriesMediator;
import Products.AccessoryKit;
import Products.Cab;
import Products.Car;
import Products.Engine;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.value.ObservableValue;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MainPageViewModel implements Observer {
    FactoriesMediator mediator;
    private SimpleIntegerProperty Period_Access;
    private SimpleIntegerProperty Period_Cab;
    private SimpleIntegerProperty Period_Eng;
    private SimpleIntegerProperty Period_Car;
    private SimpleIntegerProperty Period_Dealer;

    public FactoriesMediator getMediator() {return mediator;}

    public MainPageViewModel() {
        mediator = new FactoriesMediator("cpp_lab5_front/src/Config.json");
        mediator.addObserver(this);
        mediator.getThreadPool().addQueueObserver(this);

        Period_Access = new SimpleIntegerProperty((int)(mediator.getAccessoryFactoryManager()[0].getPeriodInMilliseconds() / 1000));
        Period_Cab = new SimpleIntegerProperty((int)(mediator.getCabFactoryManager()[0].getPeriodInMilliseconds() / 1000));
        Period_Eng = new SimpleIntegerProperty((int)(mediator.getEngineFactoryManager()[0].getPeriodInMilliseconds() / 1000));
        Period_Car = new SimpleIntegerProperty((int)(mediator.getCarFactoryManager()[0].getPeriodInMilliseconds() / 1000));
        Period_Dealer = new SimpleIntegerProperty((int)(mediator.getCarDealers()[0].getPeriodInMilliseconds() / 1000));

        CarStorCount = new SimpleIntegerProperty(mediator.getCarStorage().CurrentCount());
        CabStorCount = new SimpleIntegerProperty(mediator.getCabStorage().CurrentCount());
        EngStorCount = new SimpleIntegerProperty(mediator.getEngineStorage().CurrentCount());
        AccesStorCount = new SimpleIntegerProperty(mediator.getAccessoryStorage().CurrentCount());
        ProducedCarsCount = new SimpleIntegerProperty(mediator.getCarStorage().CurrentCount());
        DealerCount = new SimpleIntegerProperty(mediator.getCarDealers().length);
        TaskCount = new SimpleIntegerProperty(mediator.getThreadPool().getTaskQueue().size());

        Period_Access.addListener((ov, old_val, new_val) -> {
                for(var i : mediator.getAccessoryFactoryManager())
                {
                    i.setPeriodInMilliseconds(new_val.longValue()*1000);
                }
        });

        Period_Cab.addListener((ov, old_val, new_val) -> {
            for(var i : mediator.getCabFactoryManager())
            {
                i.setPeriodInMilliseconds(new_val.longValue()*1000);
            }
        });

        Period_Eng.addListener((ov, old_val, new_val) -> {
            for(var i : mediator.getEngineFactoryManager())
            {
                i.setPeriodInMilliseconds(new_val.longValue()*1000);
            }
        });

        Period_Car.addListener((ov, old_val, new_val) -> {
            for(var i : mediator.getCarFactoryManager())
            {
                i.setPeriodInMilliseconds(new_val.longValue()*1000);
            }
        });

        Period_Dealer.addListener((ov, old_val, new_val) -> {
            for(var i : mediator.getCarDealers())
            {
                i.setPeriodInMilliseconds(new_val.longValue()*1000);
            }
        });
    }
    SimpleIntegerProperty CarStorCount;
    SimpleIntegerProperty CabStorCount;
    SimpleIntegerProperty EngStorCount;
    SimpleIntegerProperty AccesStorCount;
    SimpleIntegerProperty ProducedCarsCount;
    SimpleIntegerProperty DealerCount;
    SimpleIntegerProperty TaskCount;

    public int getCarStorCount() {
        return CarStorCount.get();
    }

    public SimpleIntegerProperty CarStorCountProperty() {
        return CarStorCount;
    }

    public int getCabStorCount() {
        return CabStorCount.get();
    }

    public SimpleIntegerProperty CabStorCountProperty() {
        return CabStorCount;
    }

    public int getEngStorCount() {
        return EngStorCount.get();
    }

    public SimpleIntegerProperty EngStorCountProperty() {
        return EngStorCount;
    }

    public int getAccesStorCount() {
        return AccesStorCount.get();
    }

    public SimpleIntegerProperty AccesStorCountProperty() {
        return AccesStorCount;
    }

    public int getProducedCarsCount() {
        return ProducedCarsCount.get();
    }

    public SimpleIntegerProperty ProducedCarsCountProperty() {
        return ProducedCarsCount;
    }

    public int getDealerCount() {
        return DealerCount.get();
    }

    public SimpleIntegerProperty DealerCountProperty() {
        return DealerCount;
    }

    public int getTaskCount() {
        return TaskCount.get();
    }

    public SimpleIntegerProperty TaskCountProperty() {
        return TaskCount;
    }


    @Override
    public void update(EventArgs args) {
        if (args.getType() == EventType.ProductProduced) {
            var e = (ProductProducedEventArgs) args;
            if (e.getFactory().getClass() == EngineFactory.class) {
                EngStorCount.set(EngStorCount.get()+1);
            } else if (e.getFactory().getClass() == CabFactory.class) {
                CabStorCount.set(CabStorCount.get()+1);
            } else if (e.getFactory().getClass() == CarFactory.class) {
                CarStorCount.set(CarStorCount.get()+1);
                ProducedCarsCount.set(ProducedCarsCount.get() + 1);
            } else if (e.getFactory().getClass() == AccessoryFactory.class) {
                AccesStorCount.set(AccesStorCount.get()+1);
            }
        }else if(args.getType() == EventType.ProductTaken){
            var e = (ProductTakenEventArgs) args;
            if (e.getProduct().getClass() == Engine.class) {
                EngStorCount.set(EngStorCount.get()-1);
            } else if (e.getProduct().getClass() == Cab.class) {
                CabStorCount.set(CabStorCount.get()-1);
            } else if (e.getProduct().getClass() == Car.class) {
                CarStorCount.set(CarStorCount.get()-1);
            } else if (e.getProduct().getClass() == AccessoryKit.class) {
                AccesStorCount.set(AccesStorCount.get()-1);
            }
        }else if(args.getType() == EventType.CollectionChanged){
            var e = (CollectionChangedEventArgs) args;
            if(e.getCollection() == mediator.getThreadPool().getTaskQueue()){
                TaskCount.set(mediator.getThreadPool().getTaskQueue().size());
            }
        }
    }

    public long getPeriod_Access() {
        return Period_Access.get();
    }

    public SimpleIntegerProperty period_AccessProperty() {
        return Period_Access;
    }

    public long getPeriod_Cab() {
        return Period_Cab.get();
    }

    public SimpleIntegerProperty period_CabProperty() {
        return Period_Cab;
    }

    public long getPeriod_Eng() {
        return Period_Eng.get();
    }

    public SimpleIntegerProperty period_EngProperty() {
        return Period_Eng;
    }

    public long getPeriod_Car() {
        return Period_Car.get();
    }

    public SimpleIntegerProperty period_CarProperty() {
        return Period_Car;
    }

    public long getPeriod_Dealer() {
        return Period_Dealer.get();
    }

    public SimpleIntegerProperty period_DealerProperty() {
        return Period_Dealer;
    }
}
