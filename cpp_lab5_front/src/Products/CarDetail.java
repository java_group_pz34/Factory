package Products;

public abstract class CarDetail implements  FactoryProduct {
    int id;

    protected CarDetail(int id){
        this.id = id;
    }

    public int GetId(){
         return id;
     };
    abstract public String GetDescription();
}
