package Products;

public class Engine extends CarDetail {
    int HorsePower;
    double Volume; //об'єм

    public Engine(int id, int hPower, double volume){
        super(id);
        this.HorsePower = hPower;
        this.Volume = volume;
    }


    @Override
    public String GetDescription() {
        return "Engine. Horse power : "+ HorsePower + " volume " + Volume;
    }

}
