package Products;

public class Car implements FactoryProduct {
    Engine engine;
    Cab cab;
    AccessoryKit accessoryKit;
    int id;

    public Car(Engine engine, Cab cab, AccessoryKit accessoryKit, int id){
        this.engine = engine;
        this.cab = cab;
        this.accessoryKit = accessoryKit;
        this.id = id;
    }

    @Override
    public int GetId() {
        return id;
    }
}
