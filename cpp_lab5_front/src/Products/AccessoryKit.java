package Products;

public class AccessoryKit extends CarDetail {
    AccessoryKitType type;

   public AccessoryKit(int id, AccessoryKitType type){
        super(id);
        this.type = type;
    }

    @Override
    public String GetDescription() {
        return "Accessory kit. Type " + type.toString();
    }
}
