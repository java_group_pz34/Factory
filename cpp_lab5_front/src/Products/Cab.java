package Products;
public class Cab extends CarDetail {
    CabType type;
    public Cab(int id, CabType type){
       super(id);
       this.type = type;
    }

    @Override
    public String GetDescription() {
        return   "Cab. Type of cab: " + type.toString();
    }
}
