package Storage;

import Products.FactoryProduct;

import java.util.Stack;


public class FactoryProductStorage implements ProductStorage {

    private int maxCount;
    private Stack<FactoryProduct> details;

    public FactoryProductStorage(int maxCount){
        details = new Stack<FactoryProduct>();
        this.maxCount = maxCount;
    }
    public FactoryProductStorage(int maxCount, Stack<FactoryProduct> details){
        this.details = details;
        this.maxCount = maxCount;
    }

    public FactoryProduct getItem() {
        return details.pop();
    }

    @Override
    public void storeItem(FactoryProduct product) {
        details.push(product);
    }

    @Override
    public boolean canStoreItem() {
        return details.size() < maxCount;
    }

    @Override
    public boolean canGetItem() {
        return details.size() > 0;
    }

    @Override
    public int getMaxCount() {
        return maxCount;
    }

    @Override
    public int CurrentCount() {
        return  details.size();
    }
}
