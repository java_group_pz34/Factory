package Storage;

import Products.FactoryProduct;

public interface ProductStorage {
    public FactoryProduct getItem();
    public void storeItem(FactoryProduct product);
    public boolean canStoreItem();
    public boolean canGetItem();
    public int getMaxCount();
    public int CurrentCount();
}
