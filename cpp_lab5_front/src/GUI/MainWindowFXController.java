package GUI;

import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Duration;

import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

public class MainWindowFXController implements Initializable {

    @FXML private AnchorPane Frame;
    @FXML private Pane AssemblyInfoPane;
    @FXML private Pane AccessoryInfoPane;
    @FXML private Pane CabInfoPane;
    @FXML private Pane EngineInfoPane;
    @FXML private Pane ThreadPoolInfo;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    //region Animations
    public void InvokeBoxAnimation() {
        ImageView box = new ImageView();
        box.setImage(new Image(
                (new File("cpp_lab5_front/src/Resources/rsz_box.png")).toURI().toString()));

        TranslateTransition tran = new TranslateTransition();
        tran.setDuration(Duration.seconds(4));
        tran.setNode(box);

        tran.setFromX(211);
        tran.setFromY(252);
        tran.setToX(614);
        tran.setToY(479);

        tran.play();
        tran.setOnFinished(actionEvent -> Frame.getChildren().remove(box));
        Frame.getChildren().add(box);
    }

    public void InvokeEngineAnimation() {
        ImageView engine = new ImageView();
        engine.setImage(new Image(
                (new File("cpp_lab5_front/src/Resources/rsz_engine.png")).toURI().toString()));

        TranslateTransition tran = new TranslateTransition();
        tran.setDuration(Duration.seconds(4));
        tran.setNode(engine);

        tran.setFromX(1082);
        tran.setFromY(252);
        tran.setToX(614);
        tran.setToY(479);

        tran.play();
        tran.setOnFinished(actionEvent -> Frame.getChildren().remove(engine));
        Frame.getChildren().add(engine);
    }

    public void InvokeCabAnimation() {
        ImageView cab = new ImageView();
        cab.setImage(new Image(
                (new File("cpp_lab5_front/src/Resources/rsz_cab.png")).toURI().toString()));

        TranslateTransition tran = new TranslateTransition();
        tran.setDuration(Duration.seconds(4));
        tran.setNode(cab);

        tran.setFromX(575);
        tran.setFromY(164);
        tran.setToX(570);
        tran.setToY(479);

        tran.play();
        tran.setOnFinished(actionEvent -> Frame.getChildren().remove(cab));
        Frame.getChildren().add(cab);
    }

    public void InvokeCarAnimation() {
        ImageView car = new ImageView();
        car.setImage(new Image(
                (new File("cpp_lab5_front/src/Resources/rsz_mitsubishi.png")).toURI().toString()));

        TranslateTransition tran = new TranslateTransition();
        tran.setDuration(Duration.seconds(6));
        tran.setNode(car);

        tran.setFromX(460);
        tran.setFromY(570);
        tran.setToX(300);
        tran.setToY(800);

        tran.play();
        tran.setOnFinished(actionEvent -> Frame.getChildren().remove(car));
        Frame.getChildren().add(car);
    }
    //endregion

    @FXML private void ShowAssemblyFactoryInfo(MouseEvent event) {
        AssemblyInfoPane.setVisible(true);
    }
    @FXML private void ShowAccessoryFactoryInfo(MouseEvent event) {
        AccessoryInfoPane.setVisible(true);
    }
    @FXML private void ShowCabFactoryInfo(MouseEvent event) {
        CabInfoPane.setVisible(true);
    }
    @FXML private void ShowEngineFactoryInfo(MouseEvent event) {
        EngineInfoPane.setVisible(true);
    }

    //region Assembly unit info panel

    private SimpleIntegerProperty dillersCount = new SimpleIntegerProperty(0);
    public void SetDillersCount(SimpleIntegerProperty count) {
        dillersCount = count;
    }

    private SimpleIntegerProperty producedCarsCount = new SimpleIntegerProperty(0);
    public void SetProducedCarsCount(SimpleIntegerProperty count) {
        producedCarsCount = count;
    }

    private SimpleIntegerProperty storageCarsCount = new SimpleIntegerProperty(0);
    public void SetStorageCarsCount(SimpleIntegerProperty count) {
        storageCarsCount = count;
    }

    private SimpleIntegerProperty timeToBuyAuto = new SimpleIntegerProperty(10);
    public void SetTimeToBuyAutoCount(SimpleIntegerProperty count) {
        timeToBuyAuto = count;
    }

    public void initAssemblyFactoryPane() {
        AssemblyInfoPane.setPrefWidth(250);
        AssemblyInfoPane.setPrefHeight(150);
        AssemblyInfoPane.setLayoutX(870);
        AssemblyInfoPane.setLayoutY(480);
        AssemblyInfoPane.setBackground(new Background(new BackgroundFill(Color.web("#A4B0FF"), new CornerRadii(5), Insets.EMPTY)));
        Button btn = new Button("X");
        btn.setLayoutX(222.5);
        btn.setLayoutY(5.0);
        btn.setBackground(new Background(new BackgroundFill(Color.RED, new CornerRadii(5), Insets.EMPTY)));
        btn.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> AssemblyInfoPane.setVisible(false));
        AssemblyInfoPane.getChildren().add(btn);

        Label CaptionLabel = new Label("Assembly unit");
        CaptionLabel.setLayoutX(30);
        CaptionLabel.setLayoutY(10);
        CaptionLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        AssemblyInfoPane.getChildren().add(CaptionLabel);

        Label itemAmountLabel = new Label("Amount of dillers: ");
        Label itemAmount = new Label("");
        itemAmount.textProperty().set(dillersCount.getValue().toString());
        dillersCount.addListener((ov, old_val, new_val) -> {
            Platform.runLater(() -> {
                itemAmount.textProperty().setValue(new_val.toString());
            });
        });
        itemAmountLabel.setLayoutX(15);
        itemAmountLabel.setLayoutY(30);
        itemAmount.setLayoutX(130);
        itemAmount.setLayoutY(30);
        itemAmountLabel.setFont(new Font(14));
        itemAmount.setFont(new Font(14));
        AssemblyInfoPane.getChildren().add(itemAmountLabel);
        AssemblyInfoPane.getChildren().add(itemAmount);

        Label carsProducedLabel = new Label("Cars produced: ");
        Label carsProduced = new Label();
        carsProduced.textProperty().set(producedCarsCount.getValue().toString());
        producedCarsCount.addListener((ov, old_val, new_val) -> {
            Platform.runLater(() -> {
                carsProduced.textProperty().setValue(new_val.toString());
            });
        });
        carsProducedLabel.setLayoutX(15);
        carsProducedLabel.setLayoutY(50);
        carsProduced.setLayoutX(115);
        carsProduced.setLayoutY(50);
        carsProducedLabel.setFont(new Font(14));
        carsProduced.setFont(new Font(14));
        AssemblyInfoPane.getChildren().add(carsProducedLabel);
        AssemblyInfoPane.getChildren().add(carsProduced);

        Label carsInStorageLabel = new Label("Cars in storage: ");
        Label carsInStorage = new Label("");
        carsInStorage.textProperty().set(storageCarsCount.getValue().toString());
        storageCarsCount.addListener((ov, old_val, new_val) -> {
            Platform.runLater(() -> {
                carsInStorage.textProperty().setValue(new_val.toString());
            });
        });
        carsInStorageLabel.setLayoutX(15);
        carsInStorageLabel.setLayoutY(70);
        carsInStorage.setLayoutX(115);
        carsInStorage.setLayoutY(70);
        carsInStorageLabel.setFont(new Font(14));
        carsInStorage.setFont(new Font(14));
        AssemblyInfoPane.getChildren().add(carsInStorageLabel);
        AssemblyInfoPane.getChildren().add(carsInStorage);
        Label TimeLabel = new Label("Time to buy auto: ");
        Label TimeLabelValue = new Label("");
        Slider TimeSlider = new Slider();
        TimeSlider.setMin(1);
        TimeSlider.setMax(20);
        TimeSlider.valueProperty().bindBidirectional(timeToBuyAuto);
        DecimalFormat df = new DecimalFormat("###.##");
        TimeLabelValue.setText(df.format(TimeSlider.getValue()));
        TimeSlider.valueProperty().addListener((ov, old_val, new_val) ->
                TimeLabelValue.setText(df.format(timeToBuyAuto.doubleValue())));

        TimeLabel.setLayoutX(15);
        TimeLabel.setLayoutY(90);
        TimeLabelValue.setLayoutX(127);
        TimeLabelValue.setLayoutY(90);
        TimeLabel.setFont(new Font(14));
        TimeLabelValue.setFont(new Font(14));

        TimeSlider.setLayoutX(15);
        TimeSlider.setLayoutY(110);
        TimeSlider.setShowTickLabels(true);

        AssemblyInfoPane.getChildren().add(TimeLabel);
        AssemblyInfoPane.getChildren().add(TimeLabelValue);
        AssemblyInfoPane.getChildren().add(TimeSlider);
    }

    //endregion

    //region Accessory factory info panel

    private SimpleIntegerProperty AccKitsCount = new SimpleIntegerProperty(0);
    public void SetAccessoryKitsCount(SimpleIntegerProperty count) {
        AccKitsCount = count;
    }

    private SimpleIntegerProperty TimeToProdAccKit = new SimpleIntegerProperty(5);
    public void SetTimeToProdAccKit(SimpleIntegerProperty count) {
        TimeToProdAccKit = count;
    }

    public void initAccessoryFactoryInfo() {

        AccessoryInfoPane.setPrefWidth(250);
        AccessoryInfoPane.setPrefHeight(120);
        AccessoryInfoPane.setLayoutX(70);
        AccessoryInfoPane.setLayoutY(340);
        AccessoryInfoPane.setBackground(new Background(new BackgroundFill(Color.web("#A4B0FF"), new CornerRadii(5), Insets.EMPTY)));
        Button btn = new Button("X");
        btn.setLayoutX(222.5);
        btn.setLayoutY(5.0);
        btn.setBackground(new Background(new BackgroundFill(Color.RED, new CornerRadii(5), Insets.EMPTY)));
        btn.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> AccessoryInfoPane.setVisible(false));
        AccessoryInfoPane.getChildren().add(btn);


        Label CaptionLabel = new Label("Accessory storage");
        CaptionLabel.setLayoutX(30);
        CaptionLabel.setLayoutY(10);
        CaptionLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        AccessoryInfoPane.getChildren().add(CaptionLabel);

        
        Label itemAmountLabel = new Label("Amount of accessory kits: ");
        Label itemAmount = new Label("");
        itemAmount.textProperty().set(AccKitsCount.getValue().toString());
        AccKitsCount.addListener((ov, old_val, new_val) -> {
            Platform.runLater(() -> {
                itemAmount.textProperty().setValue(new_val.toString());
            });
        });
        itemAmountLabel.setLayoutX(15);
        itemAmountLabel.setLayoutY(30);
        itemAmount.setLayoutX(177);
        itemAmount.setLayoutY(30);
        itemAmountLabel.setFont(new Font(14));
        itemAmount.setFont(new Font(14));
        AccessoryInfoPane.getChildren().add(itemAmountLabel);
        AccessoryInfoPane.getChildren().add(itemAmount);

        Label TimeLabel = new Label("Time to produce accessory kit: ");
        Label TimeLabelValue = new Label("");
        Slider TimeSlider = new Slider();
        TimeSlider.setMin(1);
        TimeSlider.setMax(15);
        TimeSlider.valueProperty().bindBidirectional(TimeToProdAccKit);
        DecimalFormat df = new DecimalFormat("###.##");
        TimeLabelValue.setText(df.format(TimeSlider.getValue()));
        TimeSlider.valueProperty().addListener((ov, old_val, new_val) ->
                TimeLabelValue.setText(df.format(TimeToProdAccKit.doubleValue())));

        TimeLabel.setLayoutX(15);
        TimeLabel.setLayoutY(50);
        TimeLabelValue.setLayoutX(206);
        TimeLabelValue.setLayoutY(50);
        TimeLabel.setFont(new Font(14));
        TimeLabelValue.setFont(new Font(14));

        TimeSlider.setLayoutX(15);
        TimeSlider.setLayoutY(70);
        TimeSlider.setShowTickLabels(true);

        AccessoryInfoPane.getChildren().add(TimeLabel);
        AccessoryInfoPane.getChildren().add(TimeLabelValue);
        AccessoryInfoPane.getChildren().add(TimeSlider);
    }

    //endregion

    //region Cab factory info panel
    private SimpleIntegerProperty CabsCount = new SimpleIntegerProperty(0);
    public void SetCabsCount(SimpleIntegerProperty count) {
        CabsCount = count;
    }

    private SimpleIntegerProperty TimeToProdCab = new SimpleIntegerProperty(5);
    public void SetTimeToProdCab(SimpleIntegerProperty count) {
        TimeToProdCab = count;
    }

    public void initCabFactoryInfo() {

        CabInfoPane.setPrefWidth(250);
        CabInfoPane.setPrefHeight(120);
        CabInfoPane.setLayoutX(780);
        CabInfoPane.setLayoutY(60);
        CabInfoPane.setBackground(new Background(new BackgroundFill(Color.web("#A4B0FF"), new CornerRadii(5), Insets.EMPTY)));
        Button btn = new Button("X");
        btn.setLayoutX(222.5);
        btn.setLayoutY(5.0);
        btn.setBackground(new Background(new BackgroundFill(Color.RED, new CornerRadii(5), Insets.EMPTY)));
        btn.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> CabInfoPane.setVisible(false));
        CabInfoPane.getChildren().add(btn);

        Label CaptionLabel = new Label("Cab storage");
        CaptionLabel.setLayoutX(30);
        CaptionLabel.setLayoutY(10);
        CaptionLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        CabInfoPane.getChildren().add(CaptionLabel);

        Label itemAmountLabel = new Label("Amount of cabs: ");
        Label itemAmount = new Label("");
        itemAmount.textProperty().set(CabsCount.getValue().toString());
        CabsCount.addListener((ov, old_val, new_val) -> {
            Platform.runLater(() -> {
                itemAmount.textProperty().setValue(new_val.toString());
            });
        });
        itemAmountLabel.setLayoutX(15);
        itemAmountLabel.setLayoutY(30);
        itemAmount.setLayoutX(121);
        itemAmount.setLayoutY(30);
        itemAmountLabel.setFont(new Font(14));
        itemAmount.setFont(new Font(14));
        CabInfoPane.getChildren().add(itemAmountLabel);
        CabInfoPane.getChildren().add(itemAmount);

        Label TimeLabel = new Label("Time to produce cab: ");
        Label TimeLabelValue = new Label("");
        Slider TimeSlider = new Slider();
        TimeSlider.setMin(1);
        TimeSlider.setMax(15);
        TimeSlider.valueProperty().bindBidirectional(TimeToProdCab);
        DecimalFormat df = new DecimalFormat("###.##");
        TimeLabelValue.setText(df.format(TimeSlider.getValue()));
        TimeSlider.valueProperty().addListener((ov, old_val, new_val) ->
                TimeLabelValue.setText(df.format(TimeToProdCab.doubleValue())));

        TimeLabel.setLayoutX(15);
        TimeLabel.setLayoutY(50);
        TimeLabelValue.setLayoutX(149);
        TimeLabelValue.setLayoutY(50);
        TimeLabel.setFont(new Font(14));
        TimeLabelValue.setFont(new Font(14));

        TimeSlider.setLayoutX(15);
        TimeSlider.setLayoutY(70);
        TimeSlider.setShowTickLabels(true);

        CabInfoPane.getChildren().add(TimeLabel);
        CabInfoPane.getChildren().add(TimeLabelValue);
        CabInfoPane.getChildren().add(TimeSlider);
    }
    //endregion

    //region Engine factory info panel

    private SimpleIntegerProperty EnginesCount = new SimpleIntegerProperty(0);
    public void SetEnginesCount(SimpleIntegerProperty count) { EnginesCount = count; }

    private SimpleIntegerProperty TimeToProdEngine = new SimpleIntegerProperty(5);
    public void SetTimeToProdEngine(SimpleIntegerProperty count) {
        TimeToProdEngine = count;
    }

    public void initEngineFactoryInfo() {

        EngineInfoPane.setPrefWidth(250);
        EngineInfoPane.setPrefHeight(120);
        EngineInfoPane.setLayoutX(985);
        EngineInfoPane.setLayoutY(340);
        EngineInfoPane.setBackground(new Background(new BackgroundFill(Color.web("#A4B0FF"), new CornerRadii(5), Insets.EMPTY)));
        Button btn = new Button("X");
        btn.setLayoutX(222.5);
        btn.setLayoutY(5.0);
        btn.setBackground(new Background(new BackgroundFill(Color.RED, new CornerRadii(5), Insets.EMPTY)));
        btn.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEvent -> EngineInfoPane.setVisible(false));
        EngineInfoPane.getChildren().add(btn);

        Label CaptionLabel = new Label("Engine storage");
        CaptionLabel.setLayoutX(30);
        CaptionLabel.setLayoutY(10);
        CaptionLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        EngineInfoPane.getChildren().add(CaptionLabel);

        Label itemAmountLabel = new Label("Amount of engines: ");
        Label itemAmount = new Label("");
        itemAmount.textProperty().set(EnginesCount.getValue().toString());
        EnginesCount.addListener((ov, old_val, new_val) -> {
            Platform.runLater(() -> {
                itemAmount.textProperty().setValue(new_val.toString());
            });
        });
        itemAmountLabel.setLayoutX(15);
        itemAmountLabel.setLayoutY(30);
        itemAmount.setLayoutX(141);
        itemAmount.setLayoutY(30);
        itemAmountLabel.setFont(new Font(14));
        itemAmount.setFont(new Font(14));
        EngineInfoPane.getChildren().add(itemAmountLabel);
        EngineInfoPane.getChildren().add(itemAmount);

        Label TimeLabel = new Label("Time to produce engine: ");
        Label TimeLabelValue = new Label("");
        Slider TimeSlider = new Slider();
        TimeSlider.setMin(1);
        TimeSlider.setMax(15);
        TimeSlider.valueProperty().bindBidirectional(TimeToProdEngine);
        DecimalFormat df = new DecimalFormat("###.##");
        TimeLabelValue.setText(df.format(TimeSlider.getValue()));
        TimeSlider.valueProperty().addListener((ov, old_val, new_val) ->
                TimeLabelValue.setText(df.format(TimeToProdEngine.doubleValue())));

        TimeLabel.setLayoutX(15);
        TimeLabel.setLayoutY(50);
        TimeLabelValue.setLayoutX(169);
        TimeLabelValue.setLayoutY(50);
        TimeLabel.setFont(new Font(14));
        TimeLabelValue.setFont(new Font(14));

        TimeSlider.setLayoutX(15);
        TimeSlider.setLayoutY(70);
        TimeSlider.setShowTickLabels(true);

        EngineInfoPane.getChildren().add(TimeLabel);
        EngineInfoPane.getChildren().add(TimeLabelValue);
        EngineInfoPane.getChildren().add(TimeSlider);
    }
    //endregion

    //region Tread pool info
    private SimpleIntegerProperty TasksInThreadPool = new SimpleIntegerProperty(0);
    public void SetTasksCount(SimpleIntegerProperty count) {
        TasksInThreadPool = count;
    }

    public void initThreadPoolInfo() {
        ThreadPoolInfo.setPrefWidth(200);
        ThreadPoolInfo.setPrefHeight(50);
        ThreadPoolInfo.setBackground(new Background(new BackgroundFill(Color.web("#ACFD93"), new CornerRadii(3), Insets.EMPTY)));
        ThreadPoolInfo.setLayoutX(-1);
        ThreadPoolInfo.setLayoutY(633);

        Label CaptionLabel = new Label("Tasks in queue: ");
        CaptionLabel.setLayoutX(25);
        CaptionLabel.setLayoutY(17);
        CaptionLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        ThreadPoolInfo.getChildren().add(CaptionLabel);

        Label TasksCount = new Label("");
        TasksCount.setLayoutX(155);
        TasksCount.setLayoutY(17);
        TasksCount.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        TasksCount.textProperty().set(TasksInThreadPool.getValue().toString());
        TasksInThreadPool.addListener((ov, old_val, new_val) -> {
            Platform.runLater(() -> {
                TasksCount.textProperty().setValue(new_val.toString());
            });
        });
        ThreadPoolInfo.getChildren().add(TasksCount);
    }
    //endregion
}
