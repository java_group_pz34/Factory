package Events;

import Products.AccessoryKit;
import Products.Cab;
import Products.Engine;

public class ReadyToTakeCarDetailsEventArgs extends EventArgs {
    private Engine engine;
    private Cab cab;
    private AccessoryKit accessoryKit;

    public ReadyToTakeCarDetailsEventArgs(){
        super(EventType.ReadyToTakeCarDetails);
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Cab getCab() {
        return cab;
    }

    public void setCab(Cab cab) {
        this.cab = cab;
    }

    public AccessoryKit getAccessoryKit() {
        return accessoryKit;
    }

    public void setAccessoryKit(AccessoryKit accessoryKit) {
        this.accessoryKit = accessoryKit;
    }
}
