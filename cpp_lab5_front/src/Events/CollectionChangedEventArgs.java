package Events;

public class CollectionChangedEventArgs extends EventArgs{
    private Object collection;

    public CollectionChangedEventArgs(Object collection){
        super(EventType.CollectionChanged);
        this.collection = collection;
    }

    public Object getCollection() {
        return collection;
    }
}
