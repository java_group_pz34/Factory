package Events;

public enum EventType {
    ProductProduced,
    DealerReadyToTakeCar,
    ReadyToTakeCarDetails,
    ProductTaken,
    CollectionChanged
}
