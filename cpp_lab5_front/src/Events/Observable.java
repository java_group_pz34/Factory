package Events;

public interface Observable {
    void addObserver(Observer observer);
    void removeObserver(Observer observer);
    void notify(EventArgs args);
}
