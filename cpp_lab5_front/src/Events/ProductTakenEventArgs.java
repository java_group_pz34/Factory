package Events;

import Products.FactoryProduct;

public class ProductTakenEventArgs extends EventArgs {
    private FactoryProduct product;

    public ProductTakenEventArgs(FactoryProduct product){
        super(EventType.ProductTaken);
        this.product = product;
    }

    public FactoryProduct getProduct() {
        return product;
    }
}
