package Events;

public class EventArgs {
    private EventType type;

    public EventArgs(EventType type){
        this.type = type;
    }

    public EventType getType() {
        return type;
    }
}
