package Events;

public interface Observer {
    void update(EventArgs args);
}
