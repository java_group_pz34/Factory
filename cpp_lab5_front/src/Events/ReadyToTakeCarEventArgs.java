package Events;

import Dealers.CarDealer;

public class ReadyToTakeCarEventArgs extends EventArgs{
    private CarDealer dealer;

    public ReadyToTakeCarEventArgs(CarDealer dealer){
        super(EventType.DealerReadyToTakeCar);
        this.dealer = dealer;
    }

    public CarDealer getDealer() {
        return dealer;
    }
}
