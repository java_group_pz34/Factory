package Events;

import Factories.Factory;
import Products.FactoryProduct;

public class ProductProducedEventArgs extends EventArgs{
    private FactoryProduct product;
    private Factory factory;

    public ProductProducedEventArgs(FactoryProduct product, Factory factory) {
        super(EventType.ProductProduced);
        this.product = product;
        this.factory = factory;
    }

    public FactoryProduct getProduct() {
        return product;
    }

    public Factory getFactory() {
        return factory;
    }
}
