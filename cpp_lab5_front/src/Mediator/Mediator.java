package Mediator;

import Events.EventArgs;

public interface Mediator {
    void update(Object sender, EventArgs args) throws InterruptedException;
}
