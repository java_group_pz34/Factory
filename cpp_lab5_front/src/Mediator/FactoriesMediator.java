package Mediator;

import Dealers.CarDealer;
import Events.*;
import Factories.*;
import Helpers.FileReader;
import Products.AccessoryKit;
import Products.Cab;
import Products.Engine;
import Products.FactoryProduct;
import Storage.FactoryProductStorage;
import Storage.ProductStorage;
import ThreadPool.DefaultThreadPool;
import ThreadPool.ThreadPool;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class FactoriesMediator implements Mediator, Observable {
    FactoryManager[] AccessoryFactoryManager;
    FactoryManager[] CabFactoryManager;
    FactoryManager[] EngineFactoryManager;
    FactoryManager[] CarFactoryManager;
    ProductStorage carStorage;
    ProductStorage cabStorage;
    ProductStorage accessoryStorage;
    ProductStorage engineStorage;
    CarDealer[] carDealers;
    List<Observer> observers;

    final Object accessoryPut = new Object();
    final Object cabPut = new Object();
    final Object enginePut = new Object();
    final Object carPut = new Object();
    final Object carTake = new Object();
    final Object cabTake = new Object();
    final Object engineTake = new Object();
    final Object accessoryTake = new Object();

    ThreadPool threadPool;

    public ThreadPool getThreadPool() {
        return threadPool;
    }

    public ProductStorage getCarStorage() {
        return carStorage;
    }

    public ProductStorage getCabStorage() {
        return cabStorage;
    }

    public ProductStorage getAccessoryStorage() {
        return accessoryStorage;
    }

    public ProductStorage getEngineStorage() {
        return engineStorage;
    }

    public CarDealer[] getCarDealers() {
        return carDealers;
    }

    public FactoryManager[] getCarFactoryManager() {
        return CarFactoryManager;
    }

    public FactoryManager[] getEngineFactoryManager() {
        return EngineFactoryManager;
    }

    public FactoryManager[] getCabFactoryManager() {
        return CabFactoryManager;
    }

    public FactoryManager[] getAccessoryFactoryManager() {
        return AccessoryFactoryManager;
    }

    static Logger LOGGER;

    static {
        try (FileInputStream ins = new FileInputStream("cpp_lab5_front/src/log.config")) {
            LogManager.getLogManager().readConfiguration(ins);
            LOGGER = Logger.getLogger(FactoriesMediator.class.getName());
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
    }

    public FactoriesMediator(String configFilePath) {
        try {
            String jsonStr = FileReader.readFile(configFilePath, StandardCharsets.UTF_8);
            JSONObject json = new JSONObject(jsonStr);

            var accessoryConfig = json.getJSONObject("AccessoryStorage");
            var countOfAccessoryFactories = accessoryConfig.getInt("CountOfFactories");

            var carConfig = json.getJSONObject("CarStorage");
            var assemblyUnitConfig = json.getJSONObject("AssemblyUnit");
            var countOfCarFactories = carConfig.getInt("CountOfFactories");

            var cabConfig = json.getJSONObject("CabStorage");
            var countOfCabFactories = cabConfig.getInt("CountOfFactories");

            var engineConfig = json.getJSONObject("EngineStorage");
            var countOfEngineFactories = engineConfig.getInt("CountOfFactories");

            var countOfDealers = assemblyUnitConfig.getInt("DealerCount");

            var threadPoolConfig = json.getJSONObject("ThreadPool");
            var countOfAdditionalThreads = threadPoolConfig.getInt("CountOfThreads");
            var maxCountOfTasks = threadPoolConfig.getInt("MaxCountOfTasks");

            threadPool = new DefaultThreadPool(
                    countOfAccessoryFactories
                            + countOfCabFactories
                            + countOfEngineFactories
                            + countOfCarFactories
                            + countOfDealers
                            + countOfAdditionalThreads,
                    maxCountOfTasks);

            accessoryStorage = new FactoryProductStorage(accessoryConfig.getInt("Capacity"));
            AccessoryFactoryManager = new FactoryManager[countOfAccessoryFactories];
            for (var i = 0; i < countOfAccessoryFactories; ++i) {
                AccessoryFactoryManager[i] = new FactoryManager(
                        new AccessoryFactory(),
                        this,
                        json.getJSONObject("AccessoryFactory").getLong("TimeToProduceItem")
                );
                threadPool.execute(AccessoryFactoryManager[i]);
            }

            cabStorage = new FactoryProductStorage(cabConfig.getInt("Capacity"));
            CabFactoryManager = new FactoryManager[countOfCabFactories];
            for (var i = 0; i < countOfCabFactories; ++i) {
                CabFactoryManager[i] = new FactoryManager(
                        new CabFactory(),
                        this,
                        json.getJSONObject("CabFactory").getLong("TimeToProduceItem")
                );
                threadPool.execute(CabFactoryManager[i]);
            }

            engineStorage = new FactoryProductStorage(engineConfig.getInt("Capacity"));
            EngineFactoryManager = new FactoryManager[countOfEngineFactories];
            for (var i = 0; i < countOfEngineFactories; ++i) {
                EngineFactoryManager[i] = new FactoryManager(
                        new EngineFactory(),
                        this,
                        json.getJSONObject("EngineFactory").getLong("TimeToProduceItem")
                );
                threadPool.execute(EngineFactoryManager[i]);
            }

            carStorage = new FactoryProductStorage(carConfig.getInt("Capacity"));
            CarFactoryManager = new FactoryManager[countOfCarFactories];
            for (var i = 0; i < countOfCarFactories; ++i) {
                CarFactoryManager[i] = new FactoryManager(
                        new CarFactory(this, assemblyUnitConfig.getLong("TimeToProduceItem")),
                        this,
                        assemblyUnitConfig.getLong("TimeToProduceItem")
                );
                threadPool.execute(CarFactoryManager[i]);
            }

            carDealers = new CarDealer[countOfDealers];
            for (var i = 0; i < countOfDealers; ++i) {
                carDealers[i] = new CarDealer(this, assemblyUnitConfig.getLong("DealerRequestTime"));
                threadPool.execute(carDealers[i]);
            }

            observers = new ArrayList<>();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Object sender, EventArgs args) throws InterruptedException {
        if (args.getType() == EventType.ProductProduced) {
            reactOnProductProduced(sender, (ProductProducedEventArgs) args);
        } else if (args.getType() == EventType.DealerReadyToTakeCar) {
            reactOnReadyToTakeCar((ReadyToTakeCarEventArgs) args);
        } else if (args.getType() == EventType.ReadyToTakeCarDetails) {
            reactOnReadyToTakeDetails((ReadyToTakeCarDetailsEventArgs) args);
        }
    }

    private void reactOnReadyToTakeCar(ReadyToTakeCarEventArgs args) throws InterruptedException {
        synchronized (carTake) {
            while (!carStorage.canGetItem()) {
                Thread.sleep(1);
            }
            notify(args);
            var car = carStorage.getItem();
            LOGGER.log(Level.INFO, "Dealer got car with id: " + car.GetId());
            args.getDealer().addCar(car);
            notify(new ProductTakenEventArgs(car));
        }
    }

    private void reactOnProductProduced(Object manager, ProductProducedEventArgs args) throws InterruptedException {
        if (Arrays.stream(AccessoryFactoryManager).anyMatch(x -> x == manager)) {
            synchronized (accessoryPut) {
                while (!accessoryStorage.canStoreItem()) {
                    Thread.sleep(1);
                }
                notify(args);
                accessoryStorage.storeItem(args.getProduct());
            }
        } else if (Arrays.stream(CabFactoryManager).anyMatch(x -> x == manager)) {
            synchronized (cabPut) {
                while (!cabStorage.canStoreItem()) {
                    Thread.sleep(1);
                }
                notify(args);
                cabStorage.storeItem(args.getProduct());
            }
        } else if (Arrays.stream(EngineFactoryManager).anyMatch(x -> x == manager)) {
            synchronized (enginePut) {
                while (!engineStorage.canStoreItem()) {
                    Thread.sleep(1);
                }
                notify(args);
                engineStorage.storeItem(args.getProduct());
            }
        } else if (Arrays.stream(CarFactoryManager).anyMatch(x -> x == manager)) {
            synchronized (carPut) {

                while (!carStorage.canStoreItem()) {
                    Thread.sleep(1);
                }
                notify(args);
                carStorage.storeItem(args.getProduct());
            }
        }
    }

    private void reactOnReadyToTakeDetails(ReadyToTakeCarDetailsEventArgs args) throws InterruptedException {
        try {
            threadPool.execute(() -> {
                synchronized (engineTake) {
                    try{
                    while (args.getEngine() == null) {
                        while(!engineStorage.canGetItem()){
                            Thread.sleep(1);
                        }
                        var engine = (Engine) engineStorage.getItem();
                        args.setEngine(engine);
                        notify(new ProductTakenEventArgs(engine));
                    }}catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            threadPool.execute(() -> {
                synchronized (cabTake) {
                    try {
                        while (args.getCab() == null) {
                            while (!cabStorage.canGetItem()) {
                                Thread.sleep(1);
                            }
                            var cab = (Cab) cabStorage.getItem();
                            args.setCab(cab);
                            notify(new ProductTakenEventArgs(cab));
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            threadPool.execute(() -> {
                synchronized (accessoryTake) {
                    try {
                        while (args.getAccessoryKit() == null) {
                            while (!accessoryStorage.canGetItem()) {

                                Thread.sleep(1);

                            }
                            var accessoryKit = (AccessoryKit) accessoryStorage.getItem();
                            args.setAccessoryKit(accessoryKit);
                            notify(new ProductTakenEventArgs(accessoryKit));
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            while (args.getAccessoryKit() == null || args.getEngine() == null || args.getCab() == null) {
                Thread.sleep(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notify(EventArgs args) {
        for (var e :
                observers) {
            e.update(args);
        }
    }
}