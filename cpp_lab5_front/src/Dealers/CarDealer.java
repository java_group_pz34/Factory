package Dealers;

import Events.ReadyToTakeCarEventArgs;
import Mediator.Mediator;
import PeriodicRunnable.PeriodicRunnable;
import Products.FactoryProduct;

import java.util.ArrayList;
import java.util.List;

public class CarDealer extends PeriodicRunnable {
    private final List<FactoryProduct> cars;
    private Mediator mediator;

    public CarDealer(Mediator mediator, long periodInMilliseconds){
        super(periodInMilliseconds);
        this.mediator = mediator;
        this.cars = new ArrayList<>();
    }

    @Override
    protected void runLogic() throws InterruptedException {
        mediator.update(this, new ReadyToTakeCarEventArgs(this));
    }

    public void addCar(FactoryProduct car){
        cars.add(car);
    }
}
