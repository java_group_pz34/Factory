import Events.*;
import Factories.AccessoryFactory;
import Factories.CabFactory;
import Factories.CarFactory;
import Factories.EngineFactory;
import GUI.MainWindowFXController;
import Helpers.Mutex;
import Mediator.FactoriesMediator;
import Models.MainPageViewModel;
import Products.AccessoryKit;
import Products.Cab;
import Products.Car;
import Products.Engine;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application implements Observer {

    private Mutex animationAccMtx = new Mutex(true);
    private Mutex animationEngMtx = new Mutex(true);
    private Mutex animationCabMtx = new Mutex(true);
    private Mutex animationCarMtx = new Mutex(true);
    Thread handleAnimations1;
    Thread handleAnimations2;
    Thread handleAnimations3;
    Thread handleAnimations4;
    private MainPageViewModel model;

    @Override
    public void start(Stage stage) throws Exception {
        //region Gui initialization
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent frame = fxmlLoader.load(getClass().getResource("GUI/MainWindowFX.fxml").openStream());
        MainWindowFXController controller = fxmlLoader.getController();
        Scene scene = new Scene(frame);
        stage.setScene(scene);

        stage.setTitle("Car Factory");
        stage.setWidth(1280);
        stage.setHeight(720);
        stage.setResizable(false);

        stage.show();
        //endregion

        //region Animation threads initialization
        handleAnimations1 = new Thread(() -> {
            try {
                while (true) {
                    animationAccMtx.step();
                    Platform.runLater(() -> {
                        controller.InvokeBoxAnimation();
                    });
                    animationAccMtx.lock();
                    Thread.sleep(1);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        handleAnimations2 = new Thread(() -> {
            try {
                while (true) {
                    animationEngMtx.step();
                    Platform.runLater(() -> {
                        controller.InvokeEngineAnimation();
                    });
                    animationEngMtx.lock();
                    Thread.sleep(1);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        handleAnimations3 = new Thread(() -> {
            try {
                while (true) {
                    animationCabMtx.step();
                    Platform.runLater(() -> {
                        controller.InvokeCabAnimation();
                    });
                    animationCabMtx.lock();
                    Thread.sleep(1);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        handleAnimations4 = new Thread(() -> {
            try {
                while (true) {
                    animationCarMtx.step();
                    Platform.runLater(() -> {
                        controller.InvokeCarAnimation();
                    });
                    animationCarMtx.lock();
                    Thread.sleep(1);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        handleAnimations1.start();
        handleAnimations2.start();
        handleAnimations3.start();
        handleAnimations4.start();

        model = new MainPageViewModel();
        model.getMediator().addObserver(this);
        controller.SetAccessoryKitsCount(model.AccesStorCountProperty());
        controller.SetCabsCount(model.CabStorCountProperty());
        controller.SetDillersCount(model.DealerCountProperty());
        controller.SetEnginesCount(model.EngStorCountProperty());
        controller.SetProducedCarsCount(model.ProducedCarsCountProperty());
        controller.SetTasksCount(model.TaskCountProperty());
        controller.SetStorageCarsCount(model.CarStorCountProperty());
        controller.SetTimeToBuyAutoCount(model.period_DealerProperty());
        controller.SetTimeToProdAccKit(model.period_AccessProperty());
        controller.SetTimeToProdCab(model.period_CabProperty());
        controller.SetTimeToProdEngine(model.period_EngProperty());

        controller.initAssemblyFactoryPane();
        controller.initAccessoryFactoryInfo();
        controller.initCabFactoryInfo();
        controller.initEngineFactoryInfo();
        controller.initThreadPoolInfo();
        //endregion

        //Це тред-заглушка для провірки роботи анімацій. Цей код треба видалити


        //Всі об'єкти ініціалізовувати в цьому методі. Властивості фабрик, які треба виводити на юайку треба прив'язати
        //через відповідні сетери контроллера (controller.SetEnginesCount([Властивість об'єкта як SimpleIntegerProperty]);)
        //Анімації викликати через розблокування відповідного мютекса, з назви мютекса має бути зрозуміло.
    }

    @Override
    public void stop() {
        //Тут прописати що прога має робити при закритті (закриття потоків і тд.)
        model.getMediator().getThreadPool().stop();
        handleAnimations1.interrupt();
        handleAnimations2.interrupt();
        handleAnimations3.interrupt();
        handleAnimations4.interrupt();
    }

    public static void main(String[] args) {
        Application.launch();
    }

    @Override
    public void update(EventArgs args) {
        try {
            if (args.getType() == EventType.ProductTaken) {
                var e = (ProductTakenEventArgs) args;
                if (e.getProduct().getClass() == AccessoryKit.class) {
                    animationAccMtx.unlock();
                    Thread.sleep(4000);
                } else if (e.getProduct().getClass() == Engine.class) {
                    animationEngMtx.unlock();
                    Thread.sleep(4000);
                } else if (e.getProduct().getClass() == Cab.class) {
                    animationCabMtx.unlock();
                    Thread.sleep(4000);
                } else if (e.getProduct().getClass() == Car.class) {
                    animationCarMtx.unlock();
                    Thread.sleep(6000);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
