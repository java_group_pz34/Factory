package Factories;

import Products.Engine;

import java.util.Random;

public class EngineFactory implements Factory {
    @Override
    public Engine Produce() {
        idGenerator generator=new idGenerator();
        Random random=new Random();
        return new Engine(generator.generateUniqueId(), random.nextInt(200), random.nextDouble());
    }

}
