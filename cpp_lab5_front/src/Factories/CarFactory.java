package Factories;

import Events.ReadyToTakeCarDetailsEventArgs;
import Mediator.Mediator;
import Products.*;
import Products.FactoryProduct;
import Storage.FactoryProductStorage;
import Storage.ProductStorage;


import java.io.FileInputStream;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class CarFactory implements Factory {
    private Mediator mediator;
    private long periodToMakeCar;

    public CarFactory(Mediator mediator, long periodToMakeCar){
        this.mediator = mediator;
        this.periodToMakeCar = periodToMakeCar;
    }
    public void setPeriodToMakeCar (long x) {periodToMakeCar =x;}
    @Override
    public Car Produce() throws InterruptedException {
        var args = new ReadyToTakeCarDetailsEventArgs();
        mediator.update(this, args);
        try {
            Thread.sleep(periodToMakeCar);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new Car(args.getEngine(), args.getCab(), args.getAccessoryKit(), new idGenerator().generateUniqueId());
    }
}
