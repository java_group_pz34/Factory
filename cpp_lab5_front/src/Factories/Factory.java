package Factories;

import Products.FactoryProduct;

public interface Factory {
    public FactoryProduct Produce() throws InterruptedException;
}
