package Factories;

import Mediator.Mediator;

public class CarFactoryManager extends FactoryManager {
    public CarFactoryManager(Factory factory, Mediator mediator, long periodInMilliseconds) {
        super(factory, mediator, periodInMilliseconds);
    }

    @Override
    public void setPeriodInMilliseconds(long periodInMilliseconds) {
        this.periodInMilliseconds = periodInMilliseconds;
        ((CarFactory)factory).setPeriodToMakeCar(periodInMilliseconds);
    }
}
