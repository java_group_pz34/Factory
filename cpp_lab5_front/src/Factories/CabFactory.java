package Factories;

import Products.Cab;
import Products.CabType;

public class CabFactory implements Factory {
    @Override
    public Cab Produce() {
        idGenerator generator=new idGenerator();
        return new Cab(generator.generateUniqueId(), CabType.HATCHBACK);
    }
}
