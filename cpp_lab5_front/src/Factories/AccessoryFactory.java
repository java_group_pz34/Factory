package Factories;

import Products.AccessoryKit;
import Products.AccessoryKitType;
import Products.FactoryProduct;

public class AccessoryFactory implements Factory {

    @Override
    public AccessoryKit Produce() {
        idGenerator generator=new idGenerator();
        return new AccessoryKit(generator.generateUniqueId(), AccessoryKitType.BASIC);
    }
}
