package Factories;

import Events.EventType;
import Events.ProductProducedEventArgs;
import Mediator.Mediator;
import PeriodicRunnable.PeriodicRunnable;
import Storage.ProductStorage;

public class FactoryManager extends PeriodicRunnable {
    protected Factory factory;
    private Mediator mediator;

    public FactoryManager(Factory factory, Mediator mediator, long periodInMilliseconds) {
        super(periodInMilliseconds);

        this.factory = factory;
        this.mediator = mediator;
    }

    @Override
    protected void runLogic() throws InterruptedException {
        mediator.update(this, new ProductProducedEventArgs(factory.Produce(), factory));
    }

}
