package ThreadPool;

import Events.CollectionChangedEventArgs;
import Events.EventArgs;
import Events.Observable;
import Events.Observer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class ObservableBlockingQueue<T> implements Observable, BlockingQueue<T> {
    @Override
    public boolean add(T t) {
        var added = queue.add(t);
        if(added){
            notify(new CollectionChangedEventArgs(this));
        }

        return added;
    }

    @Override
    public boolean offer(T t) {
        var offered = queue.offer(t);
        if(offered){
            notify(new CollectionChangedEventArgs(this));
        }

        return offered;
    }

    @Override
    public T remove() {
        var e = queue.remove();
        notify(new CollectionChangedEventArgs(this));
        return e;
    }

    @Override
    public T poll() {
        var e = queue.poll();
        notify(new CollectionChangedEventArgs(this));
        return e;
    }

    @Override
    public T element() {
        return queue.element();
    }

    @Override
    public T peek() {
        return queue.peek();
    }

    @Override
    public void put(T t) throws InterruptedException {
        queue.put(t);
        notify(new CollectionChangedEventArgs(this));
    }

    @Override
    public boolean offer(T t, long timeout, TimeUnit unit) throws InterruptedException {
        var offered = queue.offer(t, timeout, unit);
        if(offered){
            notify(new CollectionChangedEventArgs(this));
        }

        return offered;
    }

    @Override
    public T take() throws InterruptedException {
        var e = queue.take();
        notify(new CollectionChangedEventArgs(this));
        return e;
    }

    @Override
    public T poll(long timeout, TimeUnit unit) throws InterruptedException {
        return queue.poll(timeout, unit);
    }

    @Override
    public int remainingCapacity() {
        return queue.remainingCapacity();
    }

    @Override
    public boolean remove(Object o) {
        var removed = queue.remove(o);
        if(removed){
            notify(new CollectionChangedEventArgs(this));
        }

        return removed;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return queue.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        var added = queue.addAll(c);
        if(added){
            notify(new CollectionChangedEventArgs(this));
        }

        return added;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        var removed = queue.removeAll(c);
        if(removed){
            notify(new CollectionChangedEventArgs(this));
        }

        return removed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return queue.retainAll(c);
    }

    @Override
    public void clear() {
        queue.clear();
    }

    @Override
    public int size() {
        return queue.size();
    }

    @Override
    public boolean isEmpty() {
        return queue.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return queue.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return queue.iterator();
    }

    @Override
    public Object[] toArray() {
        return queue.toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return queue.toArray(a);
    }

    @Override
    public int drainTo(Collection<? super T> c) {
        return drainTo(c);
    }

    @Override
    public int drainTo(Collection<? super T> c, int maxElements) {
        return queue.drainTo(c, maxElements);
    }

    @Override
    public void addObserver(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notify(EventArgs args) {
        for (var e:
             observerList) {
            e.update(args);
        }
    }

    public ObservableBlockingQueue(int maxCount){
        queue = new ArrayBlockingQueue<T>(maxCount);
        observerList = new ArrayList<>();
    }

    BlockingQueue<T> queue;
    List<Observer> observerList;
}
