package ThreadPool;

import Events.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class DefaultThreadPool implements ThreadPool {
    private ObservableBlockingQueue<Runnable> taskQueue = null;
    private List<PoolThreadWorker> runnables = new ArrayList<>();
    private boolean isStopped = false;

    public DefaultThreadPool(int noOfThreads, int maxNoOfTasks) {
        taskQueue = new ObservableBlockingQueue<Runnable>(maxNoOfTasks);

        for (int i = 0; i < noOfThreads; i++) {
            var poolThreadRunnable =
                    new PoolThreadWorker(taskQueue);

            runnables.add(new PoolThreadWorker(taskQueue));
        }
        for (var runnable : runnables) {
            new Thread(runnable).start();
        }
    }

    public synchronized void execute(Runnable task) throws Exception {
        if (this.isStopped) throw
                new IllegalStateException("ThreadPool is stopped");

        this.taskQueue.offer(task);
    }

    public synchronized void stop() {
        this.isStopped = true;
        for (var runnable : runnables) {
            runnable.doStop();
        }
    }

    public synchronized void waitUntilAllTasksFinished() {
        while (this.taskQueue.size() > 0) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public BlockingQueue<Runnable> getTaskQueue() {
        return taskQueue;
    }

    @Override
    public void addQueueObserver(Observer observer) {
        taskQueue.addObserver(observer);
    }

    @Override
    public void removeQueueObserver(Observer observer) {
        taskQueue.removeObserver(observer);
    }
}
