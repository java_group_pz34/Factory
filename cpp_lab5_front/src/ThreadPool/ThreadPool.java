package ThreadPool;

import Events.Observer;

import java.util.concurrent.BlockingQueue;

public interface ThreadPool {
    void execute(Runnable task) throws Exception;
    void stop();
    void waitUntilAllTasksFinished();
    BlockingQueue<Runnable> getTaskQueue();
    void addQueueObserver(Observer observer);
    void removeQueueObserver(Observer observer);
}
